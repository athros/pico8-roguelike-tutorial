# Part 1

## Initial setup

* Buy Voxatron, which includes PICO-8
* Start up PICO-8, figure out Alt+Enter takes it out of full screen
* Learn my way around the various screens

## Getting started

* Draw out a basic @ sprite in slot 0. Nothing too fancy.
* Learn the difference between `BTN` and `BTNP` - `BTN` is generally when the button is pressed, `BTNP` is when the key is released. 
* Create a table to hold the player - since there is no real OOP, I'm going to try and use tables to keep things concise. This might cause problems later, since each call into the table will be another token - it might be wiser to break those out into `p_x` and `p_y` rather than have them in a table. Same thing for the sprite at `P.SP`. This can also hold stats and other things that I need for the player. 
* Get the first draw call for the sprite on the screen. I learned really quickly that if you don't at least `rectfill` the screen, your sprite never clears behind it, causing drawn lines. I'm guessing that's where the behind the scenes blit is at. Add in the `rectfill` with color 5, and viola! 
* Update the `BTNP` calls to go by 8 pixels rather than 1. This will give the illusion of gridded movement.

## Things to figure out

* Diagonal movement is a thing. I haven't decided how I want to solve it, if I want to solve it. 

## Stats
* 78 of 8192 tokens.
* 22 lines.
